#include <stdio.h>
#include <stdlib.h>
//#define NULL 0
// Estructura que contiene un dato y un link

struct node
{
  int data;
  struct node *next;
}*p;
// p es un puntero global que contiene la dirección del primer nodo de la lista

int count();


// Función para eliminar un nodo de la lista

void delnode(int num)
{
  struct node *temp, *m;
  temp=p;
  while(temp!=NULL)
    {
      if(temp->data==num)
	{
	  if(temp==p)
	    {
              p=temp->next;
              free(temp);
              return;
	    }
	  else
	    {
	      m->next=temp->next;
	      free(temp);
	      return;
	    }
	}else
        {
	  m=temp;
          temp= temp->next;
        }

    }
  printf("Elemento %d no encontrado", num);
}

// Función para agregar un nodo al final de la lsta

void  append( int num )
{
  struct node *temp,*r;
  // Crear un nodo y asignarle un valor
  
  temp= (struct node *)malloc(sizeof(struct node));
  temp->data=num;
  r=(struct node *)p;

  if (p == NULL) // Si la lista está vacía crear el primer nodo
    {
      p=temp;
      p->next =NULL;
    }
  else
    {       
      // Ir hasta el final y agregar
      while( r->next != NULL)
	r=r->next;
      r->next =temp;
      r=temp;
      r->next=NULL;
    }
}

// Función para agregar un nuevo nodo al inicio 

void addbeg( int num )
{
  // Crear un nodo e insertar un valor
  
  struct node *temp;
  temp=(struct node *)malloc(sizeof(struct node));
  temp->data=num;

  // Si la lista está vacía agregar al inicio
  if ( p== NULL)
    {
      p=temp;
      p->next=NULL;
    }

  else
    {
      temp->next=p;
      p=temp;
    }
}

// Agrega un nuevo nodo después de un determinado número de nodos
void addafter(int num, int loc)
{
  int i;
  struct node *temp,*t,*r;
  r=p;       // Aquí r almacena la primera posición
  if(loc > count()+1 || loc <= 0)
    {
      printf("insertion is not possible!");
      return;
    }
  if (loc == 1)// Si la lista está vacía entonces agrega al inicio
    {
      addbeg(num);
      return;
    }
  else
    {
      for(i=1;i<loc;i++)
	{
	  t=r;   // t mantendrá en valor previo
	  r=r->next;
	}
      temp=(struct node *)malloc(sizeof(struct node));
      temp->data=num;
      t->next=temp;
      t=temp;
      t->next=r;
      return;
    }
}

// Función que muestra el contenido de la lista 
void display(struct node *r)
{
  r=p;
  if(r==NULL)
    {
      printf("No hay elementos en la lista\n\n");
      return;
    }
  // Recorre la lista entera
  while(r!=NULL)
    {
      printf(" -> %d ",r->data);
      r=r->next;
    }
  printf(" ");
}


//Función que cuenta el número de elementos en la lista
int count()
{
  struct node *n;
  int c=0;
  n=p;
  while(n!=NULL)
    {
      n=n->next;
      c++;
    }
  return c;
}
//THIS FUNCTION REVERSES A LINKED LIST
void reverse(struct node *q)
{
  struct node *m, *n,*l,*s;
  m=q;
  n=NULL;
  while(m!=NULL)
    {
      s=n;
      n=m;
      m=m->next;
      n->next=s;
    }
  p=n;
}


int main()
{
  int i;
  p=NULL;
  while(1)
    {
      printf("\n1. Ingresar un numero al inicio\n");
      printf("2. Ingresar un numero al final\n");
      printf("3. Ingresar un numero en una ubicacion particular\n");
      printf("4. Ver elementos de la lista\n");
      printf("5. Contar numero de elementos en la lista\n");
      printf("6. Eliminar un nodo en la lista\n");
      printf("7. Invertir orden de la lista\n");
      printf("8. Salir\n\n");
      
      printf("Ingrese una opcion: ");

      scanf("%d",&i); 

      switch(i)
	{
	case 1:
	  {
	    int num;
	    printf("Ingrese un numero: ");
	    scanf("%d",&num);
	    addbeg(num);
	    break;
	  }
	case 2:
	  {
	    int num;
	    printf("Ingrese un numero: ");
	    scanf("%d",&num);
	    append(num);
	    break;
	  }

	case 3:
	  {
	    int num, loc,k;
	    printf("Ingrese el numero: ");
	    scanf("%d",&num);
	    printf("Ingrese la ubicacion del numero: ");
	    scanf("%d",&loc);
	    addafter(num,loc);
	    break;
	  }  case 4:
	  {
	    struct node *n;
	    printf("\nLos elementos de la lista son: ");
	    display(n);
	    break;
	  }

	case 5:
	  {
	    struct node *n;
	    display(n);
	    printf("\nNumero total de elementos: %d",count());
	    break;
	  } case 6:
	  {
            int    num;
	    printf("Ingrese el numero a eliminar :");
	    scanf("%d",&num);
	    delnode(num);
	    break;
	  }
	case 7:
	  {
	    reverse(p);
	    display(p);
	    break;
	  }
	case 8:
	  {
	    exit(0);
	  }
	}/* end if switch */
    }/* end of while */


  return 0;
}/* end of main */

