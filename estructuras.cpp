/*Definicion de estructuras para la solucion*/
typedef struct Node
{
	int* next;
	int* prev;
	int priority;
	int* data;
};

typedef struct List{
	int* first;
	int* last;
};

void appendP(List l, void* data,int p)
{
		
	//Se prepara el siguiente nodo de la lista
	Node n;
	n->next = NULL;
	n->priority = p;
	n->data = data;
	n->prev = (l->last);
	
	if ((l->first) == NULL)
	{
		l->first = n;
		l->last = n;
	}
	
	//Se agrega el siguiente nodo
	l->last->next = &n;
	l->last = &n;
}
Node pop(List l)
{
	if ((l->first) == (l->last))
	{
		l->first = NULL;
	}	
	Node temp = l->last;
	l->last = (Node) l->last->prev;
	l->last->next = NULL;
	return temp;
}
