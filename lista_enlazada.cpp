#include <stdio.h>
#include <stdlib.h>
//#define NULL 0
// Estructura que contiene un dato y un link

struct node
{
  int data;
  struct node *next;
  int priority;
};
// p es un puntero global que contiene la dirección del primer nodo de la lista
int count(node *p)
{
  struct node *n;
  int c=0;
  n=p;
  while(n!=NULL)
    {
      n=n->next;
      c++;
    }
  return c;
}

// Función para eliminar un nodo de la lista

void delFirstNode(node *p)
{

    if (p->next != NULL){
    node * temp = p->next;
    p->data = p->next->data;
    p->next = temp->next;
    free(temp);
    }else{
    
    p->data = 0;
    p->next = NULL;
    }

}

// Función para agregar un nodo al final de la lsta

void  append(node *p,int num, int pri )
{
  struct node *temp,*r;
  // Crear un nodo y asignarle un valor
  
  temp= (struct node *)malloc(sizeof(struct node));
  temp->data=num;
  temp->priority=pri;
  r=(struct node *)p;

  if (p == NULL) // Si la lista está vacía crear el primer nodo
    {
      p=temp;
      p->next =NULL;
      
    }
  else
    {       
      // Ir hasta el final y agregar
      while( r->next != NULL)
	r=r->next;
      r->next =temp;
      r=temp;
      r->next=NULL;
    }
}

// Función para agregar un nuevo nodo al inicio 

void addbeg(node *p, int num, int pri )
{
  // Crear un nodo e insertar un valor
  
  struct node *temp;
  temp=(struct node *)malloc(sizeof(struct node));
  temp->data=num;
  temp->priority=pri;

  // Si la lista está vacía agregar al inicio
  if ( p== NULL)
    {
      p=temp;
      p->next=NULL;
    }

  else
    {
      temp->next=p;
      p=temp;
    }
}

// Agrega un nuevo nodo después de un determinado número de nodos
void addafter(node *p,int num,int pri, int loc)
{
  int i;
  struct node *temp,*t,*r;
  r=p;       // Aquí r almacena la primera posición
  if(loc > count(p)+1 || loc <= 0)
    {
      printf("insertion is not possible!");
      return;
    }
  if (loc == 1)// Si la lista está vacía entonces agrega al inicio
    {
      addbeg(p,num,pri);
      return;
    }
  else
    {
      for(i=1;i<loc;i++)
	{
	  t=r;   // t mantendrá en valor previo
	  r=r->next;
	}
      temp=(struct node *)malloc(sizeof(struct node));
      temp->data=num;
      temp->priority=pri;
      t->next=temp;
      t=temp;
      t->next=r;
      return;
    }
}

// Función que muestra el contenido de la lista 
void display(node *r)
{
  
  if(r==NULL)
    {
      printf("No hay elementos en la lista\n\n");
      return;
    }
  // Recorre la lista entera
  while(r!=NULL)
    {
      printf(" -> %d , %d ",r->data,r->priority);
      r=r->next;
    }
  printf(" ");
}


//Función que cuenta el número de elementos en la lista

//Funcion que devulve el indice del ultimo nodo con la prioridad indicada
int lastpri(node *p, int pri)
{
  if (p == NULL)
      return 1;
  struct node *n;
  int c=1;
  int flag = 0;
  n=p;
  while(n!=NULL)
    {
      if (n->priority == pri)
	flag = 1;
      if (flag == 1)
      {
              if (n->priority != pri)
	      {
		return c;
	      }
      }
      n=n->next;
      c++;
    }
  return c;
}




